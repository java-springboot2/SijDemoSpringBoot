package com.sij.demospringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SijDemoSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SijDemoSpringBootApplication.class, args);
	}

}
