package com.sij.demospringboot.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sij.demospringboot.entities.CourseEntity;
import com.sij.demospringboot.entities.StudentEntity;

@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, Long> {
	
}