package com.sij.demospringboot.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sij.demospringboot.annotations.DomainValidation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter; 

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseRequestModel {
    private String courseName; 
    private int courseDuration; 
}
