package com.sij.demospringboot.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sij.demospringboot.annotations.DomainValidation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter; 

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentRequestModel {
	private Long studentId; 
    private String studentName; 
    @DomainValidation(message="Domain must end with xyz")
    private String email=""; 
    private List<Long> courses; 
}
