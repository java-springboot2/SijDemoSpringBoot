package com.sij.demospringboot.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentResponseModel {
	private Long studentId; 
    private String studentName; 
    private String email;
}
