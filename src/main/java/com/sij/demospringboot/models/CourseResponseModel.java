package com.sij.demospringboot.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseResponseModel {
	private Long courseId; 
    private String courseName; 
    private int courseDuration;
}
