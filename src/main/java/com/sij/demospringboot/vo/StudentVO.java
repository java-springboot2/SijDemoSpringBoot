package com.sij.demospringboot.vo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentVO {
	private Long studentId; 
    private String studentName;
    private String email;
    private List<Long> courses;
}
