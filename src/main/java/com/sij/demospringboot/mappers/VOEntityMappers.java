package com.sij.demospringboot.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sij.demospringboot.entities.StudentEntity;
import com.sij.demospringboot.vo.StudentVO;



@Mapper(componentModel = "spring")
public interface VOEntityMappers {
	@Mapping(target = "name", source = "studentVO.studentName")
	@Mapping(target = "id", ignore=true)
	@Mapping(target = "courses", ignore=true)
	StudentEntity studentVOtoEntityMapper(StudentVO studentVO);
	
	@Mapping(target = "studentId", source = "studentEntity.id")
	@Mapping(target = "studentName", source = "studentEntity.name")
	@Mapping(target = "courses", ignore=true)
	StudentVO studentEntityToVOMapper(StudentEntity studentEntity);
}
