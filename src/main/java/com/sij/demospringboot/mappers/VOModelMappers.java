package com.sij.demospringboot.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.sij.demospringboot.entities.CourseEntity;
import com.sij.demospringboot.models.CourseRequestModel;
import com.sij.demospringboot.models.CourseResponseModel;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.vo.StudentVO;

@Mapper(componentModel = "spring")
public interface VOModelMappers {
	
	StudentResponseModel studentVOtoResponseModelMapper(StudentVO studentVO);
	@Mapping(target = "students", ignore = true)
	@Mapping(target = "id", ignore=true)
	CourseEntity courseModeltoEntityMapper(CourseRequestModel crm);
	List<CourseResponseModel> courseEntityListtoModelListMapper(List<CourseEntity> cel);
	@Mapping( target = "courseId", source = "courseEntity.id")
	CourseResponseModel toCourseResponseModel(CourseEntity courseEntity);
		
}
