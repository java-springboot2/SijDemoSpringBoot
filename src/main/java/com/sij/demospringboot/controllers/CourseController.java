package com.sij.demospringboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sij.demospringboot.models.CourseRequestModel;
import com.sij.demospringboot.models.CourseResponseModel;
import com.sij.demospringboot.models.StudentRequestModel;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.services.CourseService;
import com.sij.demospringboot.services.StudentService;
import com.sij.demospringboot.vo.StudentVO;

import jakarta.validation.Valid;

@RestController
public class CourseController {
	
	@Autowired
	private CourseService courseService; 
	
	@PostMapping(value="/save/course")
	public Long addCourseData(@Valid @RequestBody CourseRequestModel courseModel, Errors errors) {
		return courseService.saveCourse(courseModel);
	}
	
	@GetMapping(value="/get/courses")
	public List<CourseResponseModel> getAllCourses() {		
		return courseService.getAllCourses();
	}

}
