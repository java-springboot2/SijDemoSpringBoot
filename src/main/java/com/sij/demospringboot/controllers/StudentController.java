package com.sij.demospringboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sij.demospringboot.models.StudentRequestModel;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.services.StudentService;
import com.sij.demospringboot.vo.StudentVO;

import jakarta.validation.Valid;

@RestController
public class StudentController {
	
	@Autowired
	private StudentService studentService; 
	
	@PostMapping(value="/save/student")
	public Long addStudentData(@Valid @RequestBody StudentRequestModel studentModel, Errors errors) {
//		if(errors.hasErrors()) {
//	        new ResponseEntity<>(studentModel, HttpStatus.BAD_REQUEST);
//	    }
		StudentVO studentVO = new StudentVO();
		studentVO.setStudentName(studentModel.getStudentName());
		studentVO.setCourses(studentModel.getCourses());
		return studentService.saveStudent(studentVO);
	}
	
	@GetMapping(value="/get/student/{studentId}")
	public StudentResponseModel getStudentData(@PathVariable Long studentId) {		
		return studentService.getStudent(studentId);
	}

}
