package com.sij.demospringboot.services;

import java.util.List;

import com.sij.demospringboot.entities.StudentEntity;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.vo.StudentVO;

public interface StudentService {

	Long saveStudent(StudentVO studentVO);
	
	StudentResponseModel getStudent(Long studentId);

	List<StudentEntity> getStudents();
	
	StudentEntity updateStudent(Long studentId, String name);

	void deleteStudent(Long studentId);

}
