package com.sij.demospringboot.services;

import java.util.List;

import com.sij.demospringboot.entities.StudentEntity;
import com.sij.demospringboot.models.CourseRequestModel;
import com.sij.demospringboot.models.CourseResponseModel;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.vo.StudentVO;

public interface CourseService {

	Long saveCourse(CourseRequestModel crm);
	
	List<CourseResponseModel> getAllCourses();

}
