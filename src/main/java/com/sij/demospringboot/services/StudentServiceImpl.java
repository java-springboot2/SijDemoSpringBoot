package com.sij.demospringboot.services;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sij.demospringboot.dao.CourseRepository;
import com.sij.demospringboot.dao.StudentRepository;
import com.sij.demospringboot.entities.CourseEntity;
import com.sij.demospringboot.entities.StudentEntity;
import com.sij.demospringboot.mappers.VOEntityMappers;
import com.sij.demospringboot.mappers.VOModelMappers;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.vo.StudentVO;

@Service
public class StudentServiceImpl implements StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	VOEntityMappers voEntityMappers;
	
	@Autowired
	VOModelMappers voModelMappers;
	
	// Save Operation
	@Override
	public Long saveStudent(StudentVO studentVO) {
		StudentEntity studentEntity = voEntityMappers.studentVOtoEntityMapper(studentVO);
		Set<CourseEntity> courses = new HashSet<CourseEntity>(courseRepository.findAllById(studentVO.getCourses()));
		studentEntity.setCourses(courses);
		return studentRepository.save(studentEntity).getId();
	}
	
	// Read single operation 
	@Override
	public StudentResponseModel getStudent(Long studentId) {
		StudentEntity studentEntity = studentRepository.findById(studentId).get();
		StudentVO studentVO = voEntityMappers.studentEntityToVOMapper(studentEntity);
		return voModelMappers.studentVOtoResponseModelMapper(studentVO);
	}
	
	// Read all operation 
	@Override
	public List<StudentEntity> getStudents() {
		return (List<StudentEntity>) studentRepository.findAll();
	}

	// Update operation 
	@Override
	public StudentEntity updateStudent(Long studentId, String name) {
		StudentEntity student  = studentRepository.findById(studentId).get();
		return studentRepository.save(student);		
	}
	
	// Delete operation 
	@Override
    public void deleteStudent(Long studentId) 
    { 
    	studentRepository.deleteById(studentId); 
    } 
}
