package com.sij.demospringboot.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sij.demospringboot.dao.CourseRepository;
import com.sij.demospringboot.dao.StudentRepository;
import com.sij.demospringboot.entities.CourseEntity;
import com.sij.demospringboot.entities.StudentEntity;
import com.sij.demospringboot.mappers.VOEntityMappers;
import com.sij.demospringboot.mappers.VOModelMappers;
import com.sij.demospringboot.models.CourseRequestModel;
import com.sij.demospringboot.models.CourseResponseModel;
import com.sij.demospringboot.models.StudentResponseModel;
import com.sij.demospringboot.vo.StudentVO;

@Service
public class CourseServiceImpl implements CourseService {
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	VOEntityMappers voEntityMappers;
	
	@Autowired
	VOModelMappers voModelMappers;
	
	@Override
	public Long saveCourse(CourseRequestModel courseModel) {
		CourseEntity courseEntity = voModelMappers.courseModeltoEntityMapper(courseModel);
		return courseRepository.save(courseEntity).getId();
	}
	
	@Override
	public List<CourseResponseModel> getAllCourses() {
		List<CourseEntity> courseEntities = courseRepository.findAll();
		List<CourseResponseModel> courseModels = voModelMappers.courseEntityListtoModelListMapper(courseEntities);
		return courseModels;
	}
	
	
}
